package com.github.paulcwarren.twitter;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.github.paulcwarren.twitter.actions.Fetcher;
import com.github.paulcwarren.twitter.actions.Filter;
import com.github.paulcwarren.twitter.actions.Sorter;
import com.github.paulcwarren.twitter.actions.StockEarnings;
import com.github.paulcwarren.twitter.actions.Zacks;

public class TwitterCli extends Thread {

	private static List<TwitterAction> actions = new ArrayList<TwitterAction>();
	
	static {
		actions.add(new Fetcher());
		actions.add(new Filter());
		actions.add(new Sorter());
		actions.add(new Zacks());
		actions.add(new StockEarnings());
	}

	@Parameter(names = { "r", "--rate-limit" }, description = "Rate limit")
	private static int rateLimit = -1;
	
	@Parameter(names = "--help", help = true)
	private static boolean help = false;

	private JCommander cmdr;

	public static void main(String[] args) {
		TwitterCli consumer = new TwitterCli();
		JCommander cmdr = new JCommander(consumer);
	    cmdr.setProgramName("earnings");
		
		for (TwitterAction action : actions) {
			cmdr.addCommand(action.name(), action);
		}
		
		cmdr.parse(args);
		consumer.setJCommander(cmdr);
		if (help) {
            cmdr.usage();
            return;
        }

		RateLimitController.rateLimit = rateLimit;
		
		consumer.start();
		
		return;
	}

	public TwitterCli() {}
	
	private void setJCommander(JCommander cmdr) {
		this.cmdr = cmdr;
	}
	
	public void run() {
		String command = cmdr.getParsedCommand();
		for (TwitterAction action : actions) {
			if (command.equals(action.name())) {
				action.perform(System.in, System.out);
			}
		}
	}
}
