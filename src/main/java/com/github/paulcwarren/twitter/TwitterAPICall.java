package com.github.paulcwarren.twitter;

import twitter4j.TwitterException;

public interface TwitterAPICall {
	Object execute() throws TwitterException;
}
