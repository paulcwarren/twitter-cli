package com.github.paulcwarren.twitter.actions;

import static com.github.paulcwarren.twitter.RateLimitController.execute;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;
import com.github.paulcwarren.twitter.Units;

import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

@Parameters(commandDescription = "Search")
public class Search extends AuthorizedAction implements TwitterAction {
	
	private static final Logger logger = LoggerFactory.getLogger(Search.class);
	
	@Parameter(names = { "-n", "--number" }, description = "Number of users required")
	private int numberOfUsers = 100;

	@Parameter(names = { "-x", "--excludes" }, description = "Users to exclude from search")
	private File excludeFile;

	@Parameter(names = { "-q", "--query" }, description = "Query string")
	private String query;

	@Parameter(names = { "-lat", "--latitude" }, description = "latitude to search with")
	private double lat = -1;

	@Parameter(names = { "-long", "--longitude" }, description = "longitude to search with")
	private double lng = -1;

	@Parameter(names = { "-r", "--radius" }, description = "radius to search")
	private double radius = 5;

	@Parameter(names = { "-u", "--units" }, description = "units")
	private Units units = Units.mi;

	@Override
	public String name() {
		return "search";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		logger.info("Search");
		
		Twitter twitter = this.getTwitter();

		List<String> excludes = new ArrayList<>();
		if (excludeFile != null) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(excludeFile)))) {
				String user = null;
				while ((user = br.readLine()) != null) {
					excludes.add(user);
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		
		final twitter4j.Query query = new Query();
		
		if (this.lat != -1 && this.lng != -1) {
			query.geoCode(new GeoLocation(this.lat, this.lng), this.radius, units.name());
		}
		
		if (this.query != null) {
			query.query(this.query);
		}
		
		PipedInputStream pis = new PipedInputStream();
		PipedOutputStream pos;
		
		try {
			pos = new PipedOutputStream(pis);
		} catch (IOException e2) {
			e2.printStackTrace();
			return;
		}

//		Runnable r = new Runnable() {
//			@Override
//			public void run() {
//				queryForUsers(twitter, query, users, new OutputStreamWriter(pos));
//			}
//		};
//		Thread t = new Thread(r);
//		t.start();
		UserQueryThread t = new UserQueryThread(twitter, query, new OutputStreamWriter(pos), lat, lng, radius, units);
		t.start();

		int total = 0;
		int matched = 0;
		int excluded = 0; 
		
		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(pis))) {
				String user = null;
				while ((user = br.readLine()) != null && total < numberOfUsers) {
					logger.debug("Read " + user);
					total++;
					
					if (!excludes.contains(user)) {
//						final String tgt = user;
//						try {
//							Relationship relationship = execute(() -> {
//								return twitter.showFriendship(twitter.getScreenName(), tgt);
//							});
//							
//							if (relationship.isSourceFollowingTarget() == false) {
								writer.write(user);
								writer.write("\n");
								writer.flush();
		
								logger.debug(user);
								matched++;
//							} else {
//								logger.debug(String.format("Already friends with %s", user));
//								friends++;
//							}
//						} catch (TwitterException e) {
//							e.printStackTrace();
//						}
					} else {
						excluded++;
					}
				}
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			t.quit();
		}

		logger.info(String.format("%s total, %s matched (%s excluded)", total, matched, excluded));
	}

	private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}

		return (dist);
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
	
	protected class UserQueryThread extends Thread {
		
		private Twitter twitter; 
		private twitter4j.Query query;
		private OutputStreamWriter out;
		private double lat = -1;
		private double lng = -1;
		private double radius = 5;
		private Units units = Units.mi;

		private boolean done = false;
		
		public UserQueryThread(Twitter twitter, final twitter4j.Query query, OutputStreamWriter out, double lat, double lng, double radius, Units units) {
			this.twitter = twitter;
			this.query = query;
			this.out = out;
			this.lat = lat;
			this.lng = lng;
			this.radius = radius;
			this.units = units;
		}
		
		public void quit() {
			logger.debug("Quitting");
			done = true;
		}
		
		public void run() {
			long lastID = Long.MAX_VALUE;

			int total = 0;
			int dup = 0;
			int nodist = 0;
			int nogeo = 0;
			
			List<String> users = new ArrayList<>();
			try {
				while (!done) {
//					if (numberOfUsers - users.size() > 100) {
						query.setCount(100);
//					} else {
//						query.setCount(numberOfUsers - users.size());
//					}

					logger.debug(String.format("Querying"));
					QueryResult result = execute(() -> {
						return twitter.search(query);
					});
					logger.debug(String.format("Queried %s", result.getTweets().size()));

					for (Status t : result.getTweets()) {
						try {
							total++;
							
							// twitter isn't very good at geo proximity and we get lots of tweets outside of the search
							// criteria so let's double check and disregard those that don't actually match
							GeoLocation tweetLoc = t.getGeoLocation();
							if (tweetLoc != null) {
								double dist = distance(this.lat, this.lng, tweetLoc.getLatitude(), tweetLoc.getLongitude(), this.units.toString());
								logger.debug(String.format("Distance calculated at %.2f%s", dist, this.units.toString()));
								if (dist <= this.radius) {
									String user = t.getUser().getScreenName();
									if (users.contains(user) == false) {
										out.write(user);
										out.write("\n");
										out.flush();

										users.add(user);
									} else {
										dup++;
									}
								} else {
									nodist++;
								}
							} else {
								nogeo++;
							}
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
											
						if (t.getId() < lastID)
							lastID = t.getId();
					}
					
					logger.info(String.format("Gathered %s (%s seen, %s duplicates, %s outside geo, %s no geo)", users.size(), total, dup, nodist, nogeo));
					query.setMaxId(lastID - 1);
				}
			} catch (TwitterException te) {
				te.printStackTrace();
			}
			
			logger.info("Quit");
		}
	}
}
