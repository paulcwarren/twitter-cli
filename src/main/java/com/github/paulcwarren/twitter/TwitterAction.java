package com.github.paulcwarren.twitter;

import java.io.InputStream;
import java.io.OutputStream;

public interface TwitterAction {

	String name();
	void perform(InputStream in, OutputStream out);
	
}
