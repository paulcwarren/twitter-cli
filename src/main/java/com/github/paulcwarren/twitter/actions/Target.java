package com.github.paulcwarren.twitter.actions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;
import com.github.paulcwarren.twitter.TwitterState;
import com.github.paulcwarren.twitter.TwitterUtils;

@Parameters(commandDescription = "Target a twitter user")
public class Target implements TwitterAction {

	private static final Logger logger = LoggerFactory.getLogger(Target.class);

	@Parameter(names = { "-u", "--user" }, description = "The user to activate")
	private String user = null;
	
	@Override
	public String name() {
		return "target";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		logger.info("Target");

		OutputStreamWriter writer = new OutputStreamWriter(out);
		if (user != null) {
		    TwitterState state = TwitterUtils.load();
		    if (state.getToken(user) == null) {
		    	throw new IllegalStateException(String.format("No authorization for user %s.  Use `tc authorize`", user));
		    }
		    state.setActiveUser(user);
		    TwitterUtils.save(state);
		} else {
		    TwitterState state = TwitterUtils.load();
			try {
				writer.write(state.getActiveUser());
				writer.write("\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
