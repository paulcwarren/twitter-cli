package com.github.paulcwarren.twitter.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

@Parameters(commandDescription = "Sorter")
public class Sorter implements TwitterAction {

	private static final Logger logger = LoggerFactory.getLogger(Sorter.class);

	@Parameter(names = { "-h",
			"--header" }, description = "write header", required = false)
	private boolean header = false;

	private String user;
	
	@Override
	public String name() {
		return "sort";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		logger.info("Sorter");

		Reader reader = null;
		logger.info("Reading from stdin");
		reader = new InputStreamReader(System.in);

		List<Data> earnings = new ArrayList<>();
		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(reader)) {
				while ((user = br.readLine()) != null) {
					Data earning = new Data();
					String[] values = user.split(",");
					if (values.length == 11) {
						earning.symbol = values[0];
						earning.name = values[1];
						earning.when = values[2];
						earning.zacks.esp = Double.parseDouble(values[3]);
						earning.zacks.rank = Double.parseDouble(values[4]);
						earning.probability = Double.parseDouble(values[5]);
						earning.probabilityDir = values[6];
						earning.numQtrs = Double.parseDouble(values[7]);
						earning.totalQtrs = Double.parseDouble(values[8]);
						earning.predictedMove = Integer.parseInt(values[9]);
						earning.predictedMove7Day = Integer.parseInt(values[10]);
						earnings.add(earning);
					}
				}
			}

			Collections.sort(earnings, new SorterComparator());
			
			// header
			writer.write("Symbol,Name,AM/PM,ESP,Rank,Probability,Direction,Qtrs,Total Qtrs,Predicted Move,7 Day Predicted Move\n");
			
			// rows
			for (Data data : earnings) {
				writer.write(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", data.symbol, data.name, data.when, data.zacks.esp, data.zacks.rank,
																					data.probability, data.probabilityDir, data.numQtrs, data.totalQtrs,
																					data.predictedMove, data.predictedMove7Day));
				writer.flush();
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		logger.info(String.format("%s sorted", earnings.size()));
	}
	
	public static class SorterComparator implements Comparator<Data> {

		@Override
		public int compare(Data o1, Data o2) {
			if (o1.probability > o2.probability) 
				return -1;
			else if (o1.probability < o2.probability)
				return 1;
			else if (o1.probability == o2.probability) {
				if (o1.totalQtrs > o2.totalQtrs) {
					return -1;
				} else if (o1.totalQtrs < o2.totalQtrs) {
					return 1;
				} else if (o1.totalQtrs == o2.totalQtrs) {
					if (o1.predictedMove > o2.predictedMove) 
						return -1;
					else if (o1.predictedMove < o2.predictedMove) 
						return 1;
					else if (o1.predictedMove == o2.predictedMove) {
						if (o1.predictedMove7Day < o2.predictedMove7Day)
							return -1;
						else if (o1.predictedMove7Day > o2.predictedMove7Day)
							return 1;
						else {
							if (o1.zacks.esp > o2.zacks.esp) {
								return -1;
							} else if (o1.zacks.esp < o2.zacks.esp) {
								return 1;
							} else {
								return 0;
							}
						}
					}
				}
			}
			return 0;
		}
	}
}
