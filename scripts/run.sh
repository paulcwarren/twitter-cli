#!/bin/bash

earnings fetch --date $1 | earnings filter --key when --val pm --case-insensitive | earnings zacks | earnings se > $1.csv
earnings fetch --date $2 | earnings filter --key when --val am --case-insensitive | earnings zacks | earnings se >> $1.csv
cat $1.csv | earnings sort > $1-final.csv
