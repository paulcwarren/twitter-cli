package com.github.paulcwarren.twitter.actions;

import static com.github.paulcwarren.twitter.RateLimitController.execute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

import twitter4j.Twitter;
import twitter4j.TwitterException;

@Parameters(commandDescription = "Unnutes the set of input users")
public class Unmute extends AuthorizedAction implements TwitterAction {

	private static final Logger logger = LoggerFactory.getLogger(Unmute.class);
	
	@Parameter(description = "users to unmute")
	private List<String> users = null;
	
	private String user = null;
	
	@Override
	public String name() {
		return "unmute";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		logger.info("Unmute");

		Twitter twitter = this.getTwitter();
		
		int total = 0;
		int unmutes = 0;

		Reader reader = null;
		if (users == null) {
			logger.info("Reading from stdin");
			reader = new InputStreamReader(System.in);
		} else {
			logger.info("Reading from command line");
			String strUsers = null;
			for (String s : users) {
				if (strUsers == null)
				    strUsers = String.format("%s", s);
				else 
					strUsers = String.format("%s\n%s", strUsers, s);
			}
			reader = new StringReader(strUsers);
		}

		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(reader)) {
				while ((user = br.readLine()) != null) {
					total++;
					String log = String.format("%s", user);
					try {
						execute(() -> {
							return twitter.destroyMute(user);
						});
						log = String.format("%s unmuted", log);
						unmutes++;
					} catch (TwitterException e) {
						e.printStackTrace();
					}
					
					writer.write(user);
					writer.write("\n");
					writer.flush();
					
					logger.info(log);
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
			
		logger.info(String.format("%s processed, %s unmuted", total, unmutes));
	}
}
