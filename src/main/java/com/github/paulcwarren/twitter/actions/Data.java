package com.github.paulcwarren.twitter.actions;

public class Data {
	
	public class Zacks {
		public double esp;
		public double rank;
	}

	public String symbol;
	public String name;
	public String when;
	public Zacks zacks = new Zacks();
	public int predictedMove;
	public int predictedMove7Day;
	public double probability;
	public double numQtrs;
	public double totalQtrs;
	public String probabilityDir;
	
	@Override
	public String toString() {
		return String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s", this.symbol, this.name, this.when, this.zacks.esp, this.zacks.rank,
				this.probability, this.probabilityDir, this.numQtrs, this.totalQtrs,
				this.predictedMove, this.predictedMove7Day);
	}
	
	public static Data fromString(String strData) {
		String[] values = strData.split(",");
		if (values.length == 11) {
			Data earning = new Data();
			earning.symbol = values[0];
			earning.name = values[1];
			earning.when = values[2];
			earning.zacks.esp = Double.parseDouble(values[3]);
			earning.zacks.rank = Double.parseDouble(values[4]);
			earning.probability = Double.parseDouble(values[5]);
			earning.probabilityDir = values[6];
			earning.numQtrs = Double.parseDouble(values[7]);
			earning.totalQtrs = Double.parseDouble(values[8]);
			earning.predictedMove = Integer.parseInt(values[9]);
			earning.predictedMove7Day = Integer.parseInt(values[10]);
			return earning;
		}
		throw new IllegalStateException(String.format("Failed to deserialize %s", strData));
	}
}