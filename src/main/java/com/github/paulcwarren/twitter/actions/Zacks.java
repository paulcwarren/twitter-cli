package com.github.paulcwarren.twitter.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

@Parameters(commandDescription = "Get Zacks data")
public class Zacks implements TwitterAction {

	private static Logger logger = null;
	
	private String user = null;
	
	@Override
	public String name() {
		return "zacks";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		java.util.logging.Logger.getLogger("org.openqa.selenium.remote").setLevel(java.util.logging.Level.OFF);
		
		logger = LoggerFactory.getLogger(Fetcher.class);
		
		System.setProperty("webdriver.chrome.driver","/Users/warrep/Downloads/chromedriver");

		logger.info("Zacks");
		
		Reader reader = null;
		logger.info("Reading from stdin");
		reader = new InputStreamReader(System.in);

		WebDriver driver = new ChromeDriver(new ChromeDriverService.Builder().withSilent(true).build());
		
		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(reader)) {
				while ((user = br.readLine()) != null) {
					Data data = Data.fromString(user);

					logger.info("Fetching zacks data for " + data.symbol);
					
					try {
						driver.get("https://www.zacks.com/stock/quote/" + data.symbol);
						WebElement rankSummary = driver.findElement(By.cssSelector("div.quote_rank_summary"));
						List<WebElement> ranks = rankSummary.findElements(By.cssSelector("span.rank_chip"));
						for (WebElement rank : ranks) {
							String strRank = rank.getText(); 
							if (StringUtils.isNotEmpty(strRank.trim())) {
								try {
									data.zacks.rank = Integer.parseInt(strRank);
									break;
								} catch (Exception e) {
									System.err.println("Unexpected error parsing zank rank %s" + strRank);
								}
							}
						}
					} catch (Exception e) {
						logger.error("Unexpected error fetching Zacks data for " + data.symbol, e);
					} finally {}

					writer.write(String.format("%s\n", data.toString()));
					writer.flush();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			driver.quit();
		}
	}
}
