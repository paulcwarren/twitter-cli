package com.github.paulcwarren.twitter.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

@Parameters(commandDescription = "Filter a list by type of friendship")
public class Filter extends AuthorizedAction implements TwitterAction {

	private static final Logger logger = LoggerFactory.getLogger(Filter.class);

	@Parameter(names = { "-k", "--key" }, description = "the field to filter on")
	private String filterOn = null;

	@Parameter(names = { "-v", "--val" }, description = "the value of the field")
	private String strValue = "";
	
	@Parameter(names = { "-c", "--case-insensitive" }, description = "case insensitive")
	private boolean caseInsensitive = false;
	
	private String user = null;
	
	@Override
	public String name() {
		return "filter";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		logger.info("Filter");

		Reader reader = null;
		int total = 0;
		int filtered = 0;
		
		logger.info("Reading from stdin");
		reader = new InputStreamReader(System.in);
		
		logger.info(String.format("Filtering on %s", filterOn));

		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(reader)) {
				while ((user = br.readLine()) != null) {
					Data data = Data.fromString(user);
					
					try { 
						Field field = data.getClass().getDeclaredField(filterOn);
						Object value = field.get(data);
						
						if (caseInsensitive) {
							strValue = strValue.toLowerCase();
							value = value.toString().toLowerCase();
						}
						
						if (strValue.equals(value)) {
							writer.write(String.format("%s\n", data.toString()));
							filtered++;
						}
						total++;
					} catch (NoSuchFieldException nsfe) {
					} catch (IllegalArgumentException e) {
					} catch (IllegalAccessException e) {
					}
				}
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		logger.info(String.format("%s processed, %s filtered", total, filtered));
	}
}
