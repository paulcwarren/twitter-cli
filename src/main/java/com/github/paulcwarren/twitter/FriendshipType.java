package com.github.paulcwarren.twitter;

public enum FriendshipType {
	leaders, mutuals, followers, unrelated, excluded
}
