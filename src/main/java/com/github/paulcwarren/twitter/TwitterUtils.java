package com.github.paulcwarren.twitter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class TwitterUtils {

	public static final String consumerKey = "r1pmQa3KgiyDbyTyYwdfpGCs2";
	public static final String consumerSecret = "yskW7HmaP8LGRr09bavU3a0j13BrGrro4kPdixlOV0x4eIZEny";

	private TwitterUtils() {}
	
	private static File execPath() {
//		try {
//			return new File(TwitterUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
			return new File(System.getProperty("user.dir"));
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
//		return null;
	}
	
	public static TwitterState load() {
	    TwitterState state = null;
		ObjectMapper mapper = new ObjectMapper(); 
	    File from = new File(execPath(), ".tc"); 
	    TypeReference<TwitterState> typeRef = new TypeReference<TwitterState>() {};
		try {
			state = mapper.readValue(from, typeRef);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if (e instanceof FileNotFoundException) {
				return new TwitterState();
			}
			e .printStackTrace();
		} 
		return state;
	}
	
	public static void save(TwitterState state) {
		ObjectMapper mapper = new ObjectMapper();
	    File to = new File(execPath(), ".tc"); 
	    try {
			mapper.writeValue(to, state);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
