package com.github.paulcwarren.twitter.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

@Parameters(commandDescription = "Get Stock Earnings data")
public class StockEarnings implements TwitterAction {

	private static Logger logger = null;
	
	private String user = null;
	
	@Override
	public String name() {
		return "se";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		java.util.logging.Logger.getLogger("org.openqa.selenium.remote").setLevel(java.util.logging.Level.OFF);
		
		logger = LoggerFactory.getLogger(Fetcher.class);
		
		System.setProperty("webdriver.chrome.driver","/Users/warrep/Downloads/chromedriver");

		logger.info("Stock Earnings");
		
		Reader reader = null;
		logger.info("Reading from stdin");
		reader = new InputStreamReader(System.in);

		WebDriver driver = new ChromeDriver(new ChromeDriverService.Builder().withSilent(true).build());
		
		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(reader)) {
				while ((user = br.readLine()) != null) {
					Data data = Data.fromString(user);

					logger.info("Fetching stockearnings data for " + data.symbol);
					try {
						driver.get("https://stocksearning.com/q.aspx?Sys=" + data.symbol);
						WebElement predictedMove = driver.findElement(By.id("ContentPlaceHolder1_lblpredictedmove"));
						if (StringUtils.isEmpty(predictedMove.getText()) == false) {
							data.predictedMove = new Integer(predictedMove.getText().substring(0, predictedMove.getText().length() - 1));
						}
		
						WebElement predictedMove7Day = driver.findElement(By.id("ContentPlaceHolder1_lblPriceChageSevenDays"));
						if (StringUtils.isEmpty(predictedMove7Day.getText()) == false) {
							data.predictedMove7Day = new Integer(predictedMove7Day.getText().substring(0, predictedMove7Day.getText().length() - 1));
						}
		
						WebElement probability = driver.findElement(By.id("ContentPlaceHolder1_lbltotaltime"));
						if (StringUtils.isEmpty(probability.getText()) == false) {
							Pattern pattern = Pattern.compile("Following Earnings result, share price were (UP|DOWN) ([0-9]+) times out of last ([0-9]+)");
							Matcher matcher = pattern.matcher(probability.getText());
							if (matcher.find()) {
								data.numQtrs = new Double(matcher.group(2));
								data.totalQtrs = new Double(matcher.group(3));
								data.probability = (data.numQtrs / data.totalQtrs) * 100.00;
								BigDecimal bd = new BigDecimal(data.probability);
								bd = bd.round(new MathContext(4));
								data.probability = bd.doubleValue();
								data.probabilityDir = matcher.group(1);
							}
						}
					} catch (Exception e) {
						logger.error("Unexpected error fetching Stock Earnings data for " + data.symbol, e);
					} finally {}

					writer.write(String.format("%s\n", data.toString()));
					writer.flush();
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			driver.quit();
		}
	}
}
