package com.github.paulcwarren.twitter.actions;

import static com.github.paulcwarren.twitter.RateLimitController.execute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

@Parameters(commandDescription = "Followers")
public class Followers extends AuthorizedAction implements TwitterAction {

	private static final Logger logger = LoggerFactory.getLogger(Followers.class);

	@Parameter(description = "users to process")
	private List<String> users = null;

	@Parameter(names = {"-c","--count"}, description = "number of followers to fetch each query")
	private int count = 100;

	private String user = null;
	
	private long cursor = -1;
	
	@Override
	public String name() {
		return "followers";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		logger.info("Followers");

		Twitter twitter = this.getTwitter();
		
		int followers = 0;

		Reader reader = null;
		if (users == null) {
			logger.info("Reading from stdin");
			reader = new InputStreamReader(System.in);
		} else {
			logger.info("Reading from command line");
			String strUsers = null;
			for (String s : users) {
				if (strUsers == null)
				    strUsers = String.format("%s", s);
				else 
					strUsers = String.format("%s\n%s", strUsers, s);
			}
			reader = new StringReader(strUsers);
		}

		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			try (BufferedReader br = new BufferedReader(reader)) {
				while ((user = br.readLine()) != null) {
					followers++;
					try {
			            PagableResponseList<User> pagableFollowings;
			            do {
			            	pagableFollowings = execute(() -> {
				                return twitter.getFollowersList(user, cursor, count);
			            	});
			                for (User user : pagableFollowings) {
			                    writer.write(user.getScreenName());
			                    writer.write("\n");
			                    writer.flush();
			                    logger.info(String.format("%s", user.getScreenName()));
			                }
			            } while ((cursor = pagableFollowings.getNextCursor()) != 0);
					} catch (TwitterException te) {
						te.printStackTrace();
					}
				}
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		logger.info(String.format("%s followers", followers));
	}
}
