package com.github.paulcwarren.twitter;

import java.util.HashMap;
import java.util.Map;

import twitter4j.auth.AccessToken;

public final class TwitterState {

	private String activeUser;
	private Map<String,InternalToken> tokens = new HashMap<>();

	public TwitterState() {
	}

	public String getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(String activeUser) {
		this.activeUser = activeUser;
	}

	public void putToken(String user, AccessToken token) {
		InternalToken tok = new InternalToken(token.getToken(), token.getTokenSecret());
		getTokens().put(user, tok);
	}
	
	public AccessToken getToken(String user) {
		if (getTokens().get(user) == null) 
			return null;
		InternalToken token = getTokens().get(user);
		return new AccessToken(token.getAccessToken(), token.getAccessSecret());
	}
	
	public Map<String, InternalToken> getTokens() {
		return tokens;
	}

	public void setTokens(Map<String, InternalToken> tokens) {
		this.tokens = tokens;
	}
	
	@Override
	public String toString() {
		return "TwitterState [activeUser=" + activeUser + ", tokens=" + tokens + "]";
	}

	public static class InternalToken {
		
		private String accessToken;
		private String accessSecret;
		
		public InternalToken() {}

		public InternalToken(String accessToken, String accessSecret) {
			super();
			this.accessToken = accessToken;
			this.accessSecret = accessSecret;
		}

		public String getAccessToken() {
			return accessToken;
		}

		public void setAccessToken(String accessToken) {
			this.accessToken = accessToken;
		}

		public String getAccessSecret() {
			return accessSecret;
		}

		public void setAccessSecret(String accessSecret) {
			this.accessSecret = accessSecret;
		}

		@Override
		public String toString() {
			return "InternalToken [accessToken=" + accessToken + ", accessSecret=" + accessSecret + "]";
		}
	}
}
