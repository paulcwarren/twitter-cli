package com.github.paulcwarren.twitter;

import java.net.SocketException;

import twitter4j.RateLimitStatus;
import twitter4j.TwitterException;
import twitter4j.TwitterResponse;

public class RateLimitController {

	public static int rateLimit = -1;
	
	private static int delay = 0;
	
	@SuppressWarnings("unchecked")
	public static <T> T execute(TwitterAPICall call) throws TwitterException {
		try {
			//System.out.println("Delaying " + delay + " seconds");
			Thread.sleep(delay * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		T response = null; 
		try {
			response = (T) call.execute();
		} catch (TwitterException te) {
			if (te.getCause() instanceof SocketException) {
				response = (T) call.execute();
			} else {
				throw te;
			}
		}
		if (response != null && response instanceof TwitterResponse) {
			RateLimitStatus status = ((TwitterResponse)response).getRateLimitStatus();
			if (status != null && status.getRemaining() > 0) {
				if (rateLimit == -1) {
					delay = (Math.round(status.getSecondsUntilReset() / status.getRemaining()) + 1 /* add 1 just to be on the safe side */);
				} else {
					delay = rateLimit;
				}
			}
		}
		return response;
	}
	
}
