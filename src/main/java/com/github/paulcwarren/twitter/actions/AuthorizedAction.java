package com.github.paulcwarren.twitter.actions;

import com.github.paulcwarren.twitter.TwitterState;
import com.github.paulcwarren.twitter.TwitterUtils;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class AuthorizedAction {
	
	private Twitter twitter = null;
	
	public Twitter getTwitter() {
		if (twitter == null) {
			TwitterState state = TwitterUtils.load();
			if (state.getActiveUser() == null) {
				throw new IllegalStateException("No active user.  Use `tc target` to set an active target");
			}
			AccessToken token = state.getToken(state.getActiveUser());
			TwitterFactory factory = new TwitterFactory();
		    twitter = factory.getInstance();
		    twitter.setOAuthConsumer(TwitterUtils.consumerKey, TwitterUtils.consumerSecret);
		    twitter.setOAuthAccessToken(token);
		}
		return twitter;
	}
}
