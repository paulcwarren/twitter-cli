package com.github.paulcwarren.twitter.actions;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.github.paulcwarren.twitter.TwitterAction;

@Parameters(commandDescription = "Fetch earnings")
public class Fetcher implements TwitterAction {

	private static Logger logger = LoggerFactory.getLogger(Fetcher.class);

	@Parameter(names = { "-d", "--date" }, description = "date to fetch.", required = true)
	private String date;

	@Override
	public String name() {
		return "fetch";
	}

	@Override
	public void perform(InputStream in, OutputStream out) {
		Writer writer = new OutputStreamWriter(out);

		java.util.logging.Logger.getLogger("org.openqa.selenium.remote").setLevel(java.util.logging.Level.OFF);

		logger = LoggerFactory.getLogger(Fetcher.class);
		
		System.setProperty("webdriver.chrome.driver","/Users/warrep/Downloads/chromedriver");
		logger.info("Estimates for " + date);

		WebDriver driver = new ChromeDriver(new ChromeDriverService.Builder().withSilent(true).build());
		
		try {
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			isoFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date date2 = isoFormat.parse(date + "T06:00:00");
			
			Instant instant = date2.toInstant();
			String epochSeconds = Long.toString(instant.getEpochSecond());

			driver.get("http://www.zacks.com/earnings/earnings-calendar");
		    
		    ((JavascriptExecutor)driver).executeScript("PopulateWeeklyEvents(" + epochSeconds + ",'',true)");
		    
			Thread.sleep(3000);

			WebElement select = driver.findElement(By.name("earnings_rel_data_all_table_length"));
		    List<WebElement> options = select.findElements(By.tagName("option"));
		    for(WebElement option : options){
		        if(option.getText().equals("ALL")) {
		            option.click();
		            break;
		        }
		    }			
		    
			Thread.sleep(3000);
			
			List<Data> earnings = new ArrayList<>();
			List<WebElement> tables = driver.findElements(By.cssSelector("table#earnings_rel_data_all_table.display.dataTable.no-footer"));
		    int i=0;
		    List<WebElement> elements = tables.get(0).findElements(By.cssSelector("span.hoverquote-symbol"));
			for (WebElement element : elements) {
				Data data = new Data();
				data.symbol = element.getText();
				logger.info("Parsing earnings calendar entry " + data.symbol);
				WebElement parent = element.findElement(By.xpath("../../.."));
				try {
					WebElement name = parent.findElements(By.tagName("td")).get(1);
					data.name = name.findElement(By.tagName("span")).getAttribute("title").replaceAll(",", "");
				} catch (Exception e) {
					data.name = "Unknown";
				}
				try {
					String timing = parent.findElements(By.tagName("td")).get(3).getText();
					if ("amc".equals(timing))
						data.when = "PM";
					else 
						data.when = "AM";
				} catch (Exception e) {
					data.when = "AM";
				}
				
				// esp
				String esp = null;
				try {
					esp = parent.findElements(By.tagName("td")).get(6).getText();
					if (StringUtils.isNotEmpty(esp)) {
						if ("NA".equals(esp) == false && esp.endsWith("%")) {
							esp = esp.replaceAll(",", "");
							data.zacks.esp = Double.parseDouble(esp.substring(0, esp.length() - 2));
						}
					}
				} catch (Exception e) {
					logger.error(String.format("Unexpected error parsing zacks ESP value %s", esp), e);
				}
				earnings.add(data);
				writer.write(String.format("%s\n", data.toString()));
				writer.flush();
			}
			logger.info("Fetched " + earnings.size() + "\n");
		} catch (Exception e) {
			logger.error("Unexpected error", e);
		} finally {
			driver.quit();
		}
	}
}
